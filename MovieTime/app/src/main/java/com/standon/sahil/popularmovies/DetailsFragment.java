package com.standon.sahil.popularmovies;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.standon.sahil.popularmovies.data.LocalLibContract;
import com.standon.sahil.popularmovies.data.LocalLibDbHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


/**
 * A placeholder fragment containing a simple view.
 */
public class DetailsFragment extends Fragment {

    private String LOG_TAG = DetailsFragment.class.getSimpleName();

    String title,overview,release_date,poster;
    String vote_average;
    int id;

    List<String> authors = new ArrayList<String>(0);
    List<String> reviews = new ArrayList<String>(0);
    List<String> urls = new ArrayList<String>(0);

    protected void markFavorite(){
        LocalLibDbHelper dbHelper = new LocalLibDbHelper(getActivity().getApplicationContext());

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(LocalLibContract.FavoriteMovies.COLUMN_NAME_ID, id);
        values.put(LocalLibContract.FavoriteMovies.COLUMN_NAME_TITLE, title);
        values.put(LocalLibContract.FavoriteMovies.COLUMN_NAME_OVERVIEW, overview);
        values.put(LocalLibContract.FavoriteMovies.COLUMN_NAME_POSTER_PATH, poster);
        values.put(LocalLibContract.FavoriteMovies.COLUMN_NAME_RELEASEDT, release_date);
        values.put(LocalLibContract.FavoriteMovies.COLUMN_NAME_VOTEAVG, vote_average);

        long newRowLocalId = db.insert(LocalLibContract.FavoriteMovies.TABLE_NAME, null, values);

        db.close();
    }

    protected void removeFavorite(){
        LocalLibDbHelper dbHelper = new LocalLibDbHelper(getActivity().getApplicationContext());

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String selection = LocalLibContract.FavoriteMovies.COLUMN_NAME_ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(id) };
        db.delete(LocalLibContract.FavoriteMovies.TABLE_NAME, selection, selectionArgs);
        db.close();
    }

    public boolean checkRecord() {
        LocalLibDbHelper dbHelper = new LocalLibDbHelper(getActivity().getApplicationContext());

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String Query = "Select * from " + LocalLibContract.FavoriteMovies.TABLE_NAME + " where " + LocalLibContract.FavoriteMovies.COLUMN_NAME_ID + " = " + id;
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        db.close();
        return true;
    }

    private String url;

    private String key;

    public DetailsFragment() {
    }

    protected void makeUrl(){
        String YOUTUBE_BASE_URL = "https://www.youtube.com/watch?v=";
        url = YOUTUBE_BASE_URL + key;
        watchTrailer(url);
    }

    protected void watchTrailer(String url){
        //makeUrl(id);
        Intent browserIntent = new Intent(Intent.ACTION_VIEW);
        browserIntent.setData(Uri.parse(url));
        startActivity(browserIntent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView =  inflater.inflate(R.layout.fragment_details, container, false);

        ImageView posterView = (ImageView) rootView.findViewById(R.id.poster);

        Picasso.with(getActivity().getApplicationContext()).load(poster).into(posterView);


        TextView overviewView =  (TextView) rootView.findViewById(R.id.overview);
        overviewView.setText("\n" + overview);

        TextView releasedateView = (TextView) rootView.findViewById(R.id.release_date);
        releasedateView.setText("\nRelease : " + release_date);

        TextView voteRating = (TextView) rootView.findViewById(R.id.vote_average);
        voteRating.setText("\nRating : " + vote_average + "/10");

        TextView titleView = (TextView) rootView.findViewById(R.id.title);
        titleView.setText(title);

        Button trailerButton = (Button) rootView.findViewById(R.id.trailerButton);
        trailerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchTrailer fetchTask = new fetchTrailer();
                fetchTask.execute(Integer.toString(id));
            }
        });

        Button reviewsButton = (Button) rootView.findViewById(R.id.button_reviews);
        reviewsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchReviews reviewsTask = new fetchReviews();
                reviewsTask.execute(Integer.toString(id));
            }
        });
        final Button favButton = (Button) rootView.findViewById(R.id.mark_favorite);
        if(checkRecord() == true) {
            favButton.setBackgroundResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha);
            favButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeFavorite();
                    Toast t = Toast.makeText(getActivity().getApplicationContext(), getString(R.string.remove_favorite), Toast.LENGTH_SHORT);
                    t.show();
                    favButton.setBackgroundResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
                }
            });
        }
        else{
            favButton.setBackgroundResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
            favButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    markFavorite();
                    Toast t = Toast.makeText(getActivity().getApplicationContext(), getString(R.string.add_favorite), Toast.LENGTH_SHORT);
                    t.show();
                    favButton.setBackgroundResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha);
                }
            });
        }

        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        overview = args.getString("overview");
        title = args.getString("title");
        release_date = args.getString("release date");
        poster = args.getString("poster");
        vote_average = args.getString("vote average");
        id = args.getInt("id", 0);
        android.support.v7.app.ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        actionBar.setBackgroundDrawable(getResources().getDrawable(R.color.primary));
        actionBar.setTitle(title);
    }

    class fetchTrailer extends AsyncTask<String, Void, String>{
        @Override
        protected void onPostExecute(String s) {
            makeUrl();
            super.onPostExecute(s);
        }

        @Override
        protected String doInBackground(String... params) {

            if(params.length == 0){
                return null;
            }

            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;


            String trailerJsonStr;

            String apikey = BuildConfig.MOVIEDB_API_KEY;

            try{
                final String MOVIE_BASE_URL_BEG = "http://api.themoviedb.org/3/movie/";
                final String MOVIE_BASE_URL_END = "/videos?";
                final String APIKEY_PARAM = "api_key";
                String BASE_URL = MOVIE_BASE_URL_BEG + params[0] + MOVIE_BASE_URL_END;
                Uri builtUri = Uri.parse(BASE_URL).buildUpon()
                        .appendQueryParameter(APIKEY_PARAM, apikey)
                        .build();

                URL url = new URL(builtUri.toString());

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {

                    return null;
                }

                trailerJsonStr = buffer.toString();

                try {
                    final String TRAILER_KEY = "key";

                    JSONObject trailerJson = new JSONObject(trailerJsonStr);
                    JSONArray movieArray = trailerJson.getJSONArray("results");

                    JSONObject obj = movieArray.getJSONObject(0);
                    key  =  obj.getString(TRAILER_KEY);

                }catch (JSONException e){
                    Log.e(LOG_TAG, "Update Error ", e);
                    return null;
                }

            } catch (IOException e) {
                Log.e(LOG_TAG, "Network Error ", e);
                return null;
            }finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e(LOG_TAG, "Error closing stream", e);
                    }
                }
            }
            return null;


        }
    }

    class fetchReviews extends AsyncTask<String, Void, List<String>> {
        @Override
        protected List<String> doInBackground(String... params) {
            if(params.length == 0){
                return null;
            }

            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;


            String reviewJsonStr;

            String apikey = BuildConfig.MOVIEDB_API_KEY;

            try{
                final String MOVIE_BASE_URL_BEG = "http://api.themoviedb.org/3/movie/";
                final String MOVIE_BASE_URL_END = "/reviews?";
                final String APIKEY_PARAM = "api_key";
                String BASE_URL = MOVIE_BASE_URL_BEG + params[0] + MOVIE_BASE_URL_END;
                Uri builtUri = Uri.parse(BASE_URL).buildUpon()
                        .appendQueryParameter(APIKEY_PARAM, apikey)
                        .build();

                URL url = new URL(builtUri.toString());

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {
                    return null;
                }

                reviewJsonStr = buffer.toString();

                try {
                    final String REVIEW_AUTHOR = "author";
                    final String REVIEW_CONTENT = "content";
                    final String REVIEW_URL = "url";

                    JSONObject reviewJson = new JSONObject(reviewJsonStr);
                    JSONArray movieArray = reviewJson.getJSONArray("results");

                    for (int i = 0; i < movieArray.length(); i++) {
                        JSONObject obj = movieArray.getJSONObject(i);
                        authors.add(obj.getString(REVIEW_AUTHOR));
                        reviews.add(obj.getString(REVIEW_CONTENT));
                        urls.add(obj.getString(REVIEW_URL));
                    }
                }catch (JSONException e){
                    Log.e(LOG_TAG, "Update Error ", e);
                    return null;
                }

            } catch (IOException e) {
                Log.e(LOG_TAG, "Network Error ", e);
                return null;
            }finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e(LOG_TAG, "Error closing stream", e);
                    }
                }
            }
            return authors;
        }

        @Override
        protected void onPostExecute(List<String> strings) {
            Intent i = new Intent(getActivity().getApplicationContext(), Reviews.class);
            Bundle bund = new Bundle();
            bund.putSerializable("authors", (Serializable) authors);
            bund.putSerializable("urls", (Serializable) urls);
            bund.putSerializable("reviews", (Serializable) reviews);
            i.putExtras(bund);
            startActivity(i);
        }
    }

}
