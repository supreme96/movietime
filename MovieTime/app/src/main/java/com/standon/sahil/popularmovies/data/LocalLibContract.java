package com.standon.sahil.popularmovies.data;

import android.provider.BaseColumns;

/**
 * Created by sahil on 21/4/16.
 */
public final  class LocalLibContract {
    public static final class FavoriteMovies implements BaseColumns{
        public static final String TABLE_NAME = "movie";
        public static final String COLUMN_NAME_ID = "tmdb_id";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_OVERVIEW = "overview";
        public static final String COLUMN_NAME_POSTER_PATH="poster_path";
        public static final String COLUMN_NAME_RELEASEDT = "release_date";
        public static final String COLUMN_NAME_VOTEAVG = "vote_avg";
    }
}
