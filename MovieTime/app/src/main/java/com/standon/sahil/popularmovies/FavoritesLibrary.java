package com.standon.sahil.popularmovies;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.standon.sahil.popularmovies.data.LocalLibContract;
import com.standon.sahil.popularmovies.data.LocalLibDbHelper;

import java.util.ArrayList;
import java.util.List;


public class FavoritesLibrary extends Fragment {

    List<String> titles = new ArrayList<String>(0);
    String[] urls;
    List<String> overviews = new ArrayList<String>(0);
    List<String> release_dts = new ArrayList<String>(0);
    List<String> votes_avg = new ArrayList<String>(0);
    List<Integer> ids = new ArrayList<Integer>(0);

    public class ImageAdapter extends BaseAdapter {
        private Context mContext;

        private String[] urls;

        public ImageAdapter(Context c, String[] url) {
            mContext = c;

            urls = new String[url.length];
            urls = url.clone();

        }

        public int getCount() {
            return urls.length;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }


        public View getView(int position, View convertView, ViewGroup parent) {
            final ImageView imageView;

            if (convertView == null) {

                imageView = new ImageView(mContext);
                imageView.setLayoutParams(new GridView.LayoutParams(360, 513));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            } else {
                imageView = (ImageView) convertView;
            }

            Picasso.with(getActivity().getApplicationContext()).load(urls[position]).into(imageView);
            return imageView;
        }
    }

    public FavoritesLibrary() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        android.support.v7.app.ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        actionBar.setTitle(R.string.title_activity_favorites_library);
        actionBar.setBackgroundDrawable(getResources().getDrawable(R.color.primary));
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_favorites_library, container, false);
        readFavorite();
        GridView gridview = (GridView) rootView.findViewById(R.id.favorite_movies_grid_view);
        Toast t = Toast.makeText(getContext(), getString(R.string.library_switch_toast), Toast.LENGTH_SHORT);
        t.show();
        gridview.setAdapter(new ImageAdapter(getActivity().getApplicationContext(), urls));
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(getActivity().findViewById(R.id.container)==null){
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    Bundle args = new Bundle();
                    args.putString("overview", overviews.get(position));
                    args.putString("title", titles.get(position));
                    args.putString("vote average", votes_avg.get(position));
                    args.putString("release date", release_dts.get(position));
                    args.putString("poster", urls[position]);
                    args.putInt("id", ids.get(position));
                    Fragment detailsFragment = DetailsFragment.instantiate(getContext(), DetailsFragment.class.getName(), null);
                    detailsFragment.setArguments(args);
                    transaction.replace(R.id.fragment, detailsFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();}
                else{
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    Bundle args = new Bundle();
                    args.putString("overview", overviews.get(position));
                    args.putString("title", titles.get(position));
                    args.putString("vote average", votes_avg.get(position));
                    args.putString("release date", release_dts.get(position));
                    args.putString("poster", urls[position]);
                    args.putInt("id", ids.get(position));
                    Fragment detailsFragment = DetailsFragment.instantiate(getContext(), DetailsFragment.class.getName(), null);
                    detailsFragment.setArguments(args);
                    transaction.replace(R.id.container, detailsFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            }
        });
        return rootView;
    }

    protected void readFavorite() {
        LocalLibDbHelper dbHelper = new LocalLibDbHelper(getActivity().getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String[] projection = {
                LocalLibContract.FavoriteMovies._ID,
                LocalLibContract.FavoriteMovies.COLUMN_NAME_ID,
                LocalLibContract.FavoriteMovies.COLUMN_NAME_TITLE,
                LocalLibContract.FavoriteMovies.COLUMN_NAME_OVERVIEW,
                LocalLibContract.FavoriteMovies.COLUMN_NAME_POSTER_PATH,
                LocalLibContract.FavoriteMovies.COLUMN_NAME_RELEASEDT,
                LocalLibContract.FavoriteMovies.COLUMN_NAME_VOTEAVG
        };

        String sortOrder = LocalLibContract.FavoriteMovies._ID + " DESC";

        Cursor c = db.query(LocalLibContract.FavoriteMovies.TABLE_NAME, projection, null, null, null, null, sortOrder);
        for (int i = 0; i < c.getCount(); i++) {
            c.moveToPosition(i);
            titles.add(c.getString(c.getColumnIndex(LocalLibContract.FavoriteMovies.COLUMN_NAME_TITLE)));
            overviews.add(c.getString(c.getColumnIndex(LocalLibContract.FavoriteMovies.COLUMN_NAME_OVERVIEW)));
            release_dts.add(c.getString(c.getColumnIndex(LocalLibContract.FavoriteMovies.COLUMN_NAME_RELEASEDT)));
            votes_avg.add(c.getString(c.getColumnIndex(LocalLibContract.FavoriteMovies.COLUMN_NAME_VOTEAVG)));
            ids.add(c.getInt(c.getColumnIndex(LocalLibContract.FavoriteMovies.COLUMN_NAME_ID)));
        }

        urls = new String[c.getCount()];
        for (int i = 0; i < urls.length; i++) {
            c.moveToPosition(i);
            urls[i] = c.getString(c.getColumnIndex(LocalLibContract.FavoriteMovies.COLUMN_NAME_POSTER_PATH));
        }

        db.close();
    }
}
