package com.standon.sahil.popularmovies;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MoviesFragment extends Fragment{

    private String LOG_TAG = MoviesFragment.class.getSimpleName();
    String movieJsonString = new String();
    GridView gridview;
    List<jsonData> data = new ArrayList<jsonData>(0);
    List<String> Posters = new ArrayList<String>(0);
    private final static int size = 20;
    public void MoviesFragment() {
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.movies_fragment, menu);
    }

    public boolean isOnline() {
        Context context = getActivity().getApplicationContext();
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    @Override
    public void onStart(){
        super.onStart();
        boolean bool = isOnline();
        if(bool == true){
            updateMovies();
        }
        else{
            Toast t = Toast.makeText(getActivity().getApplicationContext(), getString(R.string.network_error_toast), Toast.LENGTH_LONG);
            t.show();
        }

    }

    public void updateMovies(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String movie_sort_pref = prefs.getString(getString(R.string.sort_order_key), "popularity.desc");
        fetchMovieTask moviedata = new fetchMovieTask();
        moviedata.execute(movie_sort_pref);
    }

    public String getPoster(String str){

            final String POSTER_BASE_URL = "http://image.tmdb.org/t/p/";
            final String size = "w342";

            String temp_url = new String();
            temp_url = temp_url.concat(POSTER_BASE_URL);
            temp_url = temp_url.concat(size);
            temp_url = temp_url.concat(str);
            return temp_url;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        gridview = (GridView) rootView.findViewById(R.id.movies_grid_view);
        Toast t = Toast.makeText(getContext(), getString(R.string.library_switch_toast), Toast.LENGTH_SHORT);
        t.show();
        return rootView;
    }

    public class ImageAdapter extends BaseAdapter {
        private Context mContext;

        private String[] urls;
        public ImageAdapter(Context c, String[] url) {
            mContext = c;

            urls = new String[Posters.size()];
            urls = url.clone();

        }

        public int getCount() {
            return urls.length;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }


        public View getView(int position, View convertView, ViewGroup parent) {
            final ImageView imageView;

            if (convertView == null) {

                imageView = new ImageView(mContext);
                imageView.setLayoutParams(new GridView.LayoutParams(360, 513));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            } else {
                imageView = (ImageView) convertView;
            }

            Picasso.with(getActivity().getApplicationContext()).load(urls[position]).into(imageView);
            return imageView;
        }
    }

    class fetchMovieTask extends AsyncTask<String, Void, List<String>>{



        @Override
        protected List<String> doInBackground(String ... params) {

            if(params.length == 0){
                return null;
            }

            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;


            String movieJsonStr;

            String apikey = BuildConfig.MOVIEDB_API_KEY;

            try{
                final String MOVIE_BASE_URL = "http://api.themoviedb.org/3/discover/movie?";
                final String SORT_TYPE_PARAM = "sort_by";
                final String APIKEY_PARAM = "api_key";
                Uri builtUri = Uri.parse(MOVIE_BASE_URL).buildUpon()
                        .appendQueryParameter(SORT_TYPE_PARAM, params[0])
                        .appendQueryParameter(APIKEY_PARAM, apikey)
                        .build();

                URL url = new URL(builtUri.toString());

                Log.v(LOG_TAG, "url="+url);

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {

                    return null;
                }
                movieJsonStr = buffer.toString();

                movieJsonString=movieJsonString.concat(movieJsonStr);


                try {
                    final String MOV_RESULT = "results";
                    final String MOV_POSTER = "poster_path";
                    final String MOV_OVERVIEW = "overview";
                    final String MOV_RELEASE = "release_date";
                    final String MOV_VOTES = "vote_average";
                    final String MOV_TITLE = "original_title";
                    final String MOV_ID = "id";

                    JSONObject movieJson = new JSONObject(movieJsonString);
                    JSONArray movieArray = movieJson.getJSONArray(MOV_RESULT);
                    String poster_path , original_title, overview, release_date;
                    double vote_average;
                    int id;
                    for(int i = 0; i < size; i++ ){

                        JSONObject obj = movieArray.getJSONObject(i);
                        poster_path  =  obj.getString(MOV_POSTER);
                        original_title =  obj.getString(MOV_TITLE);
                        overview = obj.getString(MOV_OVERVIEW);
                        release_date = obj.getString(MOV_RELEASE);
                        vote_average = obj.getDouble(MOV_VOTES);
                        id = obj.getInt(MOV_ID);
                        jsonData data_obj = new jsonData();
                        data_obj.original_title = original_title;
                        data_obj.poster_path = poster_path;
                        data_obj.overview = overview;
                        data_obj.release_date = release_date;
                        data_obj.vote_average = vote_average;
                        data_obj.id = id;
                        data.add(i, data_obj);

                    }

                    for(int i = 0; i <size; i++){
                        String path;
                        path=data.get(i).poster_path;
                        Posters.add(path);
                    }

                    return  Posters;

                }catch (JSONException e){
                    Log.e(LOG_TAG, "Update Error ", e);
                    return null;
                }

            } catch (IOException e) {
                Log.e(LOG_TAG, "Network Error ", e);
                return null;
            }finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e(LOG_TAG, "Error closing stream", e);
                    }
                }
            }
        }

        @Override
        protected void onPostExecute(List<String> Posters){

                final String[] url = new String[size];
                for (int i = 0; i < size; i++) {
                    url[i] = getPoster(Posters.get(i));
                }


            gridview.setAdapter(new ImageAdapter(getActivity().getApplicationContext(),url));
            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if(getActivity().findViewById(R.id.container)==null) {
                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        Bundle args = new Bundle();
                        args.putString("overview", data.get(position).overview);
                        args.putString("title", data.get(position).original_title);
                        args.putString("vote average", Double.toString(data.get(position).vote_average));
                        args.putString("release date", data.get(position).release_date);
                        args.putString("poster", url[position]);
                        args.putInt("id", data.get(position).id);
                        Fragment detailsFragment = DetailsFragment.instantiate(getContext(), DetailsFragment.class.getName(), null);
                        detailsFragment.setArguments(args);
                        transaction.replace(R.id.fragment, detailsFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }else{
                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        Bundle args = new Bundle();
                        args.putString("overview", data.get(position).overview);
                        args.putString("title", data.get(position).original_title);
                        args.putString("vote average", Double.toString(data.get(position).vote_average));
                        args.putString("release date", data.get(position).release_date);
                        args.putString("poster", url[position]);
                        args.putInt("id", data.get(position).id);
                        Fragment detailsFragment = DetailsFragment.instantiate(getContext(), DetailsFragment.class.getName(), null);
                        detailsFragment.setArguments(args);
                        transaction.replace(R.id.container, detailsFragment);
                        transaction.addToBackStack(null);transaction.commit();
                    }
                }
            });
        }
    }
}


