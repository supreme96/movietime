package com.standon.sahil.popularmovies;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


class ReviewContent{
    String author;
    String url;
    String review;
}

public class Reviews extends ActionBarActivity {

    private ArrayList<ReviewContent> content = new ArrayList<>(0);
    private List<String> authors_list;
    private List<String> reviews_list;
    private List<String> urls_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reviews);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getString(R.string.app_name));
        actionBar.setBackgroundDrawable(getResources().getDrawable(R.color.primary));
        Bundle bundle = getIntent().getExtras();

            authors_list = (List<String>) bundle.getSerializable("authors");
            urls_list = (List<String>) bundle.getSerializable("urls");
            reviews_list = (List<String>) bundle.getSerializable("reviews");
            for(int i=0;i<authors_list.size();i++){
                ReviewContent temp = new ReviewContent();
                temp.author = authors_list.get(i);
                temp.review = reviews_list.get(i);
                temp.url = urls_list.get(i);
                content.add(temp);
            }


        RecyclerView recyclerView = (RecyclerView) findViewById(
                R.id.review_recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new ContentAdapter(content));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return(super.onOptionsItemSelected(item));
    }
}


class ContentAdapter extends RecyclerView.Adapter<ContentAdapter.ContentViewHolder> {

    private List<ReviewContent> mcontent;

    public ContentAdapter(List<ReviewContent> content){
        mcontent = content;
    }

    @Override
    public ContentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View contentView = inflater.inflate(R.layout.card_view, parent, false);
        ContentViewHolder viewHolder = new ContentViewHolder(contentView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ContentViewHolder holder, int position) {
        ReviewContent temp_content = mcontent.get(position);
        holder.text_author.setText("Reviewer : " + temp_content.author);
        holder.text_review.setText(temp_content.review);
        holder.text_url.setText(temp_content.url);
    }

    @Override
    public int getItemCount() {
        return mcontent.size();
    }

    public class ContentViewHolder extends RecyclerView.ViewHolder {
        protected TextView text_author;
        protected TextView text_review;
        protected TextView text_url;

        public ContentViewHolder(View v) {
            super(v);
            text_author = (TextView) v.findViewById(R.id.Author);
            text_review = (TextView) v.findViewById(R.id.Review);
            text_url = (TextView) v.findViewById(R.id.Url);
        }
    }
}