package com.standon.sahil.popularmovies.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.standon.sahil.popularmovies.data.LocalLibContract.FavoriteMovies;

/**
 * Created by sahil on 21/4/16.
 */
public class LocalLibDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "LocalLib.db";

    public LocalLibDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        final String SQL_CREATE_FAVORITES_TABLE = "CREATE TABLE " + FavoriteMovies.TABLE_NAME + " (" +
                FavoriteMovies._ID + " INTEGER PRIMARY KEY, " +
                FavoriteMovies.COLUMN_NAME_ID + " INTEGER NOT NULL, " +
                FavoriteMovies.COLUMN_NAME_TITLE + " TEXT NOT NULL, " +
                FavoriteMovies.COLUMN_NAME_OVERVIEW + " TEXT NOT NULL, " +
                FavoriteMovies.COLUMN_NAME_POSTER_PATH + " TEXT NOT NULL, " +
                FavoriteMovies.COLUMN_NAME_RELEASEDT + " TEXT NOT NULL, " +
                FavoriteMovies.COLUMN_NAME_VOTEAVG + " TEXT NOT NULL, " +
                "UNIQUE (" + FavoriteMovies.COLUMN_NAME_ID + ") ON CONFLICT REPLACE );";
        sqLiteDatabase.execSQL(SQL_CREATE_FAVORITES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

    }
}
