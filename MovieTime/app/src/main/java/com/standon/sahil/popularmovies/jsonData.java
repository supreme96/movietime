package com.standon.sahil.popularmovies;

import java.io.Serializable;

/**
 * Created by sahil on 7/5/16.
 */public class jsonData implements Serializable{
    public String original_title;
    public String poster_path ;
    public String overview;
    public String release_date;
    public double vote_average;
    public int id;
}
