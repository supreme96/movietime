#Movie Time

This Android Application allows users to see information of latest movies running in theaters  along with their trailers and critics reviews. Users can also bookmark their movies of interest to view later. Users can either view the latest movies or the top rated movies. The application is optimized for tablets as well.

The application makes uses of the TMDB.org API. Hence it's working is dependent on procuring an API key from the TMDB.org website. Please procure an API key and insert it into the build.gradle(module:app) file in Andriod Studio to successfully build the app from this source code.

![Alt text](/screenshots/latest.png?raw=true "Discover the Latest Movies")
![Alt text](/screenshots/favorites.png?raw=true "Mark Favorites to view them later")
![Alt text](/screenshots/details.png?raw=true "Movie Details with synopsis and trailer")
![Alt text](/screenshots/reviews.png?raw=true "Critics Reviews")
![Alt text](/screenshots/tablet.png?raw=true "Optimized for tablets")
